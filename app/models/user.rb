class User < ApplicationRecord

    has_secure_password
    mount_uploader :image, ImageUploader
    ##Relations
    has_many :posts, dependent: :destroy
    has_many :like_posts, dependent: :destroy
    has_many :like_comments, dependent: :destroy
    has_many :comments, dependent: :destroy
    # has_many :follows, dependent: :destroy

    # has_many :liked_posts,class_name: "Post", through: :like_posts
    has_many :follows
    has_and_belongs_to_many :followers, join_table: "follows", foreign_key: "followed_id", class_name: "User", association_foreign_key: "follower_id"
    has_and_belongs_to_many :followeds, join_table: "follows", foreign_key: "follower_id", class_name: "User", association_foreign_key: "followed_id"


    ##Validations
    validates :name, :email, :birthdate, :gender, :contact_phone, :nickname, presence:true 
    validates :email, uniqueness:true
    validates :password, :password_confirmation, length: {minimum: 6}, if: :password
    validates :contact_phone, length: {is: 11}, numericality: {only_integer:true}
    validate  :underage?

    ##Functions
    enum gender:{
        "Feminino":0,
        "Masculino":1
    }
    enum role:{
        "not_valid":0,
        "standard":1,
        "admin":2
    }

    # before_create :generate_validation_token

    def age
        (((Date.today- birthdate).to_i)/365.25).to_i
    end 
    def underage?
        if birthdate.nil? || age < 18
            errors.add(:underage, "Voce e menor de idade")
        end
    end 
    def destroy_follows
        followeds = Follow.where(follower_id: id)
        followeds.each do |t|
            t.destroy
        end
        followers = Follow.where(followed_id: id)
        followers.each do |t|
            t.destroy
        end
    end 
    def generate_validation_token
        self.validate_token = generate_random_token
        self.validate_token_expiry_at = Time.now + 2.minutes
        self.save
        # self.validate_token = generate_random_token
        # self.validate_token_expiry_at = Time.now + 2.minutes
        # self.save

    end
    
    def validate_user? (token)
        if validate_token_expiry_at > Time.now
            self.role = 1
            self.validate_token = nil
            return true if self.save
        else
            return false
        end
    end
    def reset_password_complete?(password, password_confirmation)
        self.password = password
        self.password_confirmation = password_confirmation
        self.validate_token = nil
        return true if self.save
    end
    private
        def generate_random_token
            SecureRandom.alphanumeric(15)
        end
end

