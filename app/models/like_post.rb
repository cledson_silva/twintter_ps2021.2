class LikePost < ApplicationRecord
  ##Relations
  belongs_to :user
  belongs_to :post
  
  ##Validation
  validate :already_liked?
  ##Functions
  def already_liked?
    if post.users.include?(user)
      errors.add(:already_liked,"Voce ja deu like nesse postd")
    end
  end 
end
