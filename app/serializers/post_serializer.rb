class PostSerializer < ActiveModel::Serializer
  attributes :id, :content_text
  belongs_to :user
end
